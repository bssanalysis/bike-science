# BikeScience

BikeScience is a open source project that relies on advanced Data Science techniques for investigating Bike-Sharing Systems. 

## Getting started

BikeScience allows urban planners to analyze and understand the impact of Bike-Sharing Systems in their cities. 
Such knowledge can be used to support future planning and improvements regarding bike-sharing. 

Using BikeScience you can investigate any other interesting issues you may want to know about. 
The instructions below explain the basic requirements to get start with the project. 

## Prerequisites

This project is being developed with Python 3.7.

### Native libraries

* `libspatialindex`

```bash
sudo apt-get install libspatialindex-c4v5
```

### Python packages

* seaborn
* matplotlib
* folium
* pandas
* geopandas
* rtree
* jupyter (or jupyterlab)
* numpy 
* scipy 
* sklearn 

You can use [pip](https://pypi.org/project/pip/) or [conda](https://conda.io/) to install these python packages.

```bash
conda install folium
```

## Usage

You can use the current notebooks in the **boston-od-trips** folder to see examples of analyses we've already done. 
* **Descriptive-Statistics** analyzes bycicle trips of the Boston's bike-sharing system regarding *average use over the years*, *age*, *trip duration*, *speed*, and *distance*.
* **Flows** shows the most common bicycle flows for different periods of a day.
* **Layers** relates bycicle flows with the infrastructures of bicycle and public transportation of the Greater Boston area.
* **Hub-Analysis** shows regions of the cities in which there are more starts ans ends of bike trips.
* **Distance-Analysis** explores trips by their distances.
* **Fast-Cyclists** outlines a profile of speeders, which are more exposed to accidents.


## License

This project is licensed under the [MPL 2.0 License](https://www.mozilla.org/en-US/MPL/2.0/) - see the LICENSE file for details.
