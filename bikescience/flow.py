import numpy as np
import geopandas as gpd
from shapely.geometry import Point
import folium
import math
from collections import namedtuple
from .arrow import draw_arrow
from .stations import draw_stations


# Cache variables
last_grid = None
last_stations = None
grid_and_stations = None


def merge_grid_and_stations(grid, stations):
    """
    ** Internal use function **
    
    Performs a spatial join between grid and stations GeoDataframes. 
    Result is cached in flow.grid_and_stations module variable and reused for subsequent calls for the same grid and stations
    objects.
    Parameters:
      grid - contains the rectangles of the grid. 
             GeoDataframe is obtained from some_grid.geodataframe() method call (some_grid is a grid.Grid object)
      stations - a stations list with their location points. Obtained from stations.from_trips(trips) function call.
    """
    global last_grid, last_stations, grid_and_stations
    if grid_and_stations is None or grid is not last_grid or stations is not last_stations:
        grid_and_stations = gpd.sjoin(grid.geodataframe(), stations, op='contains')
        grid_and_stations.rename(columns={'index_right': 'station id'}, inplace=True)
        last_grid = grid
        last_stations = stations


def normalize(group):
    """
    ** Internal use function **
    
    Normalizes numerical data between 0 and 1, but avoiding zero value for mathematical reasons (minimum is transformed to 1e-10).
    """
    return np.interp(group, (group.min(), group.max()), (1e-10, 1))
    
    
def od_countings(trips, grid, stations):
    """
    Calculation of the Origin-Destination matrix.
    For each (A, B) pair of cells in the grid, counts the trips from A to B (flow count) and determines the "mass center" points
    of the stations in A and in B, based on the weighted average use of the stations, for arrow drawing purposes.
    """
    # caching for multiple calls on the same datasets
    merge_grid_and_stations(grid, stations)
    
    # total count per (origin, destination) pair of stations 
    od_station_counts = trips.groupby(['start station id', 'end station id'], as_index=False) \
                                     .agg({'tripduration': 'count'})
    
    # origin cells, given the stations
    with_origin_cells = od_station_counts.merge(grid_and_stations, left_on='start station id', right_on='station id') \
                        [['i', 'j', 'tripduration', 'start station id', 'end station id']]
    with_origin_cells.columns = ['i_start', 'j_start', 'trip counts', 'start station id', 'end station id']

    # destination cells, given the stations
    with_od_cells = with_origin_cells.merge(grid_and_stations, left_on='end station id', right_on='station id') \
                      [['i_start', 'j_start', 'i', 'j', 'start station id', 'end station id', 'trip counts']]
    with_od_cells.columns = ['i_start', 'j_start', 'i_end', 'j_end', 'start station id', 'end station id', 
                               'trip counts']
    
    
    # ORIGIN ----------------------------------------------------------------------------------------------------
    
    # origin stations coordinates
    with_orig_coords = with_od_cells.merge(stations, left_on='start station id', right_index=True) \
                       [['i_start', 'j_start', 'i_end', 'j_end', 'start station id', 'lat', 'lon', 
                         'trip counts']]
    
    # CENTER OF MASS
    
    # normalizing trip counts as weights between 0 and 1
    per_od_cells = with_orig_coords.groupby(['i_start', 'j_start', 'i_end', 'j_end'])
    with_orig_coords['normalized_counts'] = per_od_cells['trip counts'].transform(normalize)
    
    # ponderate coordinates by normalized weights
    with_orig_coords['pondered_lat'] = with_orig_coords['lat'] * with_orig_coords['normalized_counts']
    with_orig_coords['pondered_lon'] = with_orig_coords['lon'] * with_orig_coords['normalized_counts']
    
    # totals per (origin, destination) pair of grid cells 
    orig_mass_centers = with_orig_coords.groupby(['i_start', 'j_start', 'i_end', 'j_end'], as_index=False) \
                          .agg({'normalized_counts': 'sum', 'pondered_lat': 'sum', 'pondered_lon': 'sum'})
    orig_mass_centers['center of mass'] = orig_mass_centers.apply(
            lambda row: Point(row['pondered_lon'] / row['normalized_counts'], 
                              row['pondered_lat'] / row['normalized_counts']),
            axis=1)
    

    # DESTINATION ----------------------------------------------------------------------------------------------------
    with_dest_coords = with_od_cells.merge(stations, left_on='end station id', right_index=True) \
                       [['i_start', 'j_start', 'i_end', 'j_end', 'end station id', 'lat', 'lon', 
                         'trip counts']]

    per_od_cells = with_dest_coords.groupby(['i_start', 'j_start', 'i_end', 'j_end'])
    with_dest_coords['normalized_counts'] = per_od_cells['trip counts'].transform(normalize)
    with_dest_coords['pondered_lat'] = with_dest_coords['lat'] * with_dest_coords['normalized_counts']
    with_dest_coords['pondered_lon'] = with_dest_coords['lon'] * with_dest_coords['normalized_counts']
    
    dest_mass_centers = with_dest_coords.groupby(['i_start', 'j_start', 'i_end', 'j_end'], as_index=False) \
                        .agg({'normalized_counts': 'sum', 'pondered_lat': 'sum', 'pondered_lon': 'sum'})
    dest_mass_centers['center of mass'] = dest_mass_centers.apply(
            lambda row: Point(row['pondered_lon'] / row['normalized_counts'], 
                              row['pondered_lat'] / row['normalized_counts']),
            axis=1)
    
    
    # MERGING ALL TOGETHER ---------------------------------------------------------------------------------------
    od_counts = with_od_cells.groupby(['i_start', 'j_start', 'i_end', 'j_end'], as_index=False) \
                             .agg({'trip counts': 'sum'}) \
                             .merge(orig_mass_centers, on=['i_start', 'j_start', 'i_end', 'j_end']) \
                              [['i_start', 'j_start', 'i_end', 'j_end', 'trip counts', 'center of mass']] \
                             .merge(dest_mass_centers, on=['i_start', 'j_start', 'i_end', 'j_end']) \
                              [['i_start', 'j_start', 'i_end', 'j_end', 'trip counts', 'center of mass_x', 
                                'center of mass_y']]
    od_counts.columns = ['i_start', 'j_start', 'i_end', 'j_end', 'trip counts', 'origin', 'destination']
    return od_counts


def od_countings_simple(trips, grid, stations):
    """
    Calculation of the Origin-Destination matrix.
    For each (A, B) pair of cells in the grid, counts the trips from A to B (flow count).
    """
    
    merge_grid_and_stations(grid, stations)

    trips['start geometry'] = trips.apply(lambda row: Point(row['start station longitude'], row['start station latitude']), axis=1)
    trips_geoframe = gpd.GeoDataFrame(trips, crs={'init': 'epsg:4326'})

    # total count per (origin, destination) pair of stations 
    od_station_counts = trips.groupby(['start station id', 'end station id'], as_index=False) \
                                     .agg({'tripduration': 'count'})
    # origin cells, given the stations
    with_origin_cells = od_station_counts.merge(grid_and_stations, left_on='start station id', right_on='station id') \
                        [['i', 'j', 'tripduration', 'end station id']]
    with_origin_cells.columns = ['i_start', 'j_start', 'trip counts', 'end station id']

    # destination cells, given the stations
    with_dest_cells = with_origin_cells.merge(grid_and_stations, left_on='end station id', right_on='station id') \
                      [['i_start', 'j_start', 'i', 'j', 'trip counts']]
    with_dest_cells.columns = ['i_start', 'j_start', 'i_end', 'j_end', 'trip counts']

    # total count per (origin, destination) pair of grid cells 
    od_cell_counts = with_dest_cells.groupby(['i_start', 'j_start', 'i_end', 'j_end'], as_index=False) \
                                    .agg({'trip counts': 'sum'})
    # geometries (for plotting)
    od_counts = od_cell_counts.merge(grid.geodataframe(), left_on=['i_start', 'j_start'], right_on=['i', 'j']) \
                              .merge(grid.geodataframe(), left_on=['i_end', 'j_end'], right_on=['i', 'j']) \
                            [['i_start', 'j_start', 'geometry_x', 'i_end', 'j_end', 'geometry_y', 'trip counts']]
    od_counts.columns = ['i_start', 'j_start', 'origin', 'i_end', 'j_end', 'destination', 'trip counts']
    od_counts['origin'] = od_counts['origin'].apply(lambda cell: Point(cell.centroid.x, cell.centroid.y))
    od_counts['destination'] = od_counts['destination'].apply(lambda cell: Point(cell.centroid.x, cell.centroid.y))
    
    return od_counts


def flow_map(fmap, od_df, grid, stations, minimum=-1, maximum=-1, show=4, radius=1.0):
    '''
    Creates a flow map based on the given list of trips.
    
    Parameters
    fmap: a Folium map
    od_df: origin-destination countings dataframe for the regions of the city, calculated by od_* functions in this module
    grid: a Grid object
    stations: a GeoDataframe with Point objects representing stations
    minimum: only draw arrows for the flows that are larger than this minimum number of trips
    maximum: only draw arrows for the flows that are smaller than this maximum number of trips
    show: a measure of the portion of flows to show. Typically between 2 and 5. 
    '''

    # eliminate round-trips to the same station, which are not considering in this analysis
    filtered = od_df[(od_df['i_start'] != od_df['i_end']) | (od_df['j_start'] != od_df['j_end'])]

    if maximum == -1:
        maximum = filtered['trip counts'].max()
    if minimum == -1:
        minimum = maximum / show
        
    total_trips = filtered['trip counts'].sum()

    filtered = filtered[((filtered['trip counts'] >= minimum) & (filtered['trip counts'] <= maximum))]

    shown_trips = 0
    
    for idx, row in filtered.iterrows():
        num_trips = row['trip counts']
        
        shown_trips += num_trips
        weight = math.ceil( (num_trips-minimum)/maximum * 10)
        if weight == 0: weight = 1
        
        o1 = row['origin'].y
        o2 = row['origin'].x
        d1 = row['destination'].y
        d2 = row['destination'].x

        draw_arrow(fmap, o1, o2, d1, d2, 
                   text=str(num_trips) + ' bike trips',
                   weight=weight,
                   radius_fac=radius)

    geodf = grid.geodataframe()
    geodf = geodf[(geodf['i'] == grid.n // 5) & (geodf['j'] == grid.n // 5)]
    cell = geodf['geometry'].iloc[0]
    folium.Marker(location=[cell.centroid.y, cell.centroid.x],
                  popup='Showing '+ str(shown_trips)+' trips representing ' + 
                         str(int(100*shown_trips/total_trips)) + '% of all trips'
                 ).add_to(fmap)    


def show_flow_map(od, grid, stations):
    """
    Convenience function to quickly create and show a flow map given OD, grid and stations dataframes.
    """
    fmap = grid.map_around(zoom=13, plot_grid=True)
    flow_map(fmap, od, grid, stations, show=5, radius=2.0)
    draw_stations(fmap, stations)
    return fmap